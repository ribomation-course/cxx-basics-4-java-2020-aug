#pragma once

#include <cmath>
#include "Shape.hxx"

class Circle : public Shape {
    using super = Shape;
    const double PI = 4 * atan(1);
    int          radius;
public:
    Circle(int radius)
            : super{"circ"}, radius{radius} {
        cout << "circ{" << radius << "} @ " << this << endl;
    }

    ~Circle() {
        cout << "~Circle() @ " << this << endl;
    }

    double area() const override {
        return PI * radius * radius;
    }

    string toString() const override {
        ostringstream buf;
        buf << super::toString() << "{" << radius << "}";
        return buf.str();
    }
};
