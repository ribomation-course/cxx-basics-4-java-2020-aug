#include <iostream>
#include <string>
#include <random>
#include <vector>
#include "Shape.hxx"
#include "Rect.hxx"
#include "Circle.hxx"
#include "Triangle.hxx"
#include "Square.hxx"

using namespace std;

auto mkShape() -> Shape*;
auto mkShapes(int n) -> vector<Shape*>;


int main(int numArgs, char* args[]) {
    int const N      = (numArgs <= 1) ? 1 : stoi(args[1]);
    auto      shapes = mkShapes(N);

    cout << "---------------------\n";
    for (auto s : shapes) cout << *s << '\n';

    cout << "---------------------\n";
    for (auto s : shapes) delete s;

    return 0;
}

auto mkShape() -> Shape* {
    static random_device r;
    auto                 nextShape = uniform_int_distribution<int>{1, 4};
    auto                 nextValue = uniform_int_distribution<int>{1, 10};

    switch (nextShape(r)) {
        case 1:
            return new Rect{nextValue(r), nextValue(r)};
        case 2:
            return new Circle{nextValue(r)};
        case 3:
            return new Triangle{nextValue(r), nextValue(r)};
        case 4:
            return new Square{nextValue(r)};
        default:
            break;
    }
    throw domain_error("WTF: this should not happen");
}

auto mkShapes(int n) -> vector<Shape*> {
    auto shapes = vector<Shape*>{};
    shapes.reserve(n);
    while (n-- > 0) shapes.push_back(mkShape());
    return shapes;
}
