#pragma once

#include "Rect.hxx"


class Square : public Rect {
public:
    Square(int side)
            : Rect("square", side, side) {
        cout << "Square(" << side << ") " << this << endl;
    }

    ~Square() {
        cout << "~Square() " << this << endl;
    }
};
