#include <iostream>
#include <cctype>
#include <string>
#include <vector>

using namespace std;
using namespace std::literals;

int main() {
    auto        words = vector<string>{};
    for (string word; cin >> word;) words.push_back(word);

    cout << "(1) ";
    for (auto const& w : words) cout << w << " ";
    cout << endl;

    cout << "(2) ";
    auto totLen = 0;
    for (auto const& w : words) totLen += w.size();
    cout << "Total length: " << totLen << endl;

    cout << "(3) ";
    words.insert(words.begin(), "FIRST!");
    for (auto const& w : words) cout << w << " ";
    cout << endl;

    cout << "(4) ";
    for (auto& word : words) {
        auto result = string{};
        for (auto ch : word) if (isalpha(ch)) result += ch;
        word = result;
    }
    for (auto const& w : words) if (!w.empty()) cout << w << " ";
    cout << endl;

    cout << "(5) ";
    while (!words.empty()) {
        if (!words.back().empty()) cout << " " << words.back();
        words.pop_back();
    }
    cout << endl;

    return 0;
}
