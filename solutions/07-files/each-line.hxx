#include <iostream>
#include <fstream>
#include <stdexcept>
#include <functional>
#include <string>
#include <utility>

namespace ribomation::io {
    using std::istream;
    using std::ifstream;
    using std::getline;
    using std::invalid_argument;
    using std::function;
    using std::string;

    using Online = function<void(string const&)>;

    void each_line(istream& is, Online const& onLine) {
        for (auto line = string{}; getline(is, line);) {
            onLine(line);
        }
    }

    void each_line(string const& filename, Online const& onLine) {
        auto file = ifstream{filename};
        if (!file) throw invalid_argument{"cannot open " + filename};
        each_line(file, onLine);
    }

}

