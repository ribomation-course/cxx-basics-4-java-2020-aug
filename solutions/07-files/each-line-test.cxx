#include <iostream>
#include "each-line.hxx"

using namespace std;
using namespace std::literals;
using namespace ribomation::io;

int main() {
    auto filename = "../each-line.hxx"s;
    auto lineno   = 1U;
    each_line(filename, [&lineno](auto const& line) {
        cout << "[" << lineno++ <<"] "<< line << endl;
    });
    return 0;
}

