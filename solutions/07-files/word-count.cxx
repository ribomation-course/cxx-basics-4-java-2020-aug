#include <iostream>
#include <sstream>
#include <iomanip>
#include "each-line.hxx"

using namespace std;
using namespace std::literals;
using namespace ribomation::io;

struct Counts {
    unsigned lines = 0;
    unsigned words = 0;
    unsigned chars = 0;
};

auto count(istream& in) -> Counts {
    auto countWords = [](string const& line) -> unsigned {
        auto        cnt = 0U;
        auto        buf = istringstream{line};
        for (string word; buf >> word;) ++cnt;
        return cnt;
    };

    auto cnt = Counts{};
    each_line(in, [&cnt, &countWords](auto const& line) {
        ++cnt.lines;
        cnt.words += countWords(line);
        cnt.chars += line.size() + 1;
    });

    return cnt;
}

int main(int argc, char** argv) {
    if (argc <= 1) {
        auto stdin = count(cin);
        cout << stdin.lines << " " << stdin.words << " " << stdin.chars << endl;
    } else {
        auto      total = Counts{};
        for (auto k     = 1U; k < argc; ++k) {
            auto filename = string{argv[k]};
            auto file     = ifstream{filename};
            auto cnt      = count(file);
            cout << setw(4) << cnt.lines << " "
                 << setw(6) << cnt.words << " "
                 << setw(8) << cnt.chars << " " << filename << endl;
            total.lines += cnt.lines;
            total.words += cnt.words;
            total.chars += cnt.chars;
        }
        if (argc > 2) {
            cout << setw(4) << total.lines << " "
                 << setw(6) << total.words << " "
                 << setw(8) << total.chars << " total" << endl;
        }
    }
    return 0;
}
