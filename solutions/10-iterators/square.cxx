#include <iostream>
#include <iterator>

using namespace std;

int main() {
    istream_iterator<double> in{cin};
    istream_iterator<double> eof{};
    ostream_iterator<long>   out{cout, " "};

    for (; in != eof; ++in, ++out) {
        auto n = *in;
        *out = static_cast<long>(n * n);
    }
    cout << endl;

    return 0;
}
