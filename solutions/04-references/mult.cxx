#include <iostream>
using namespace std;

void mult(int& n) {
    n *= 42;
}

int main() {
    auto n = 10;
    cout << "before: n=" << n << endl;
    mult(n);
    cout << "after : n=" << n << endl;
    return 0;
}
