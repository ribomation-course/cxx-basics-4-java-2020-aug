#pragma once

#include <iostream>
#include <sstream>
#include <cstring>

namespace ribomation {
    using namespace std;

    class Person {
        char* name = nullptr;
        unsigned age = 0;

        static char* copystr(const char* str) {
            if (str == nullptr) return nullptr;
            return strcpy(new char[strlen(str) + 1], str);
        }

    public:
        ~Person() {
            cout << "~Person @ " << this << endl;
            delete[] name;
        }

        Person() {
            cout << "Person{} @ " << this << endl;
            name = copystr("");
        }

        Person(const char* n, unsigned a)
                : name{copystr(n)}, age{a} {
            cout << "Person{const char* " << n << ", " << a << "} @ " << this << endl;
        }

        Person(const Person& that)
                : name{copystr(that.name)}, age{that.age} {
            cout << "Person{const Person& " << &that << "} @ " << this << endl;
        }

        Person(Person&& that) noexcept
                : name{that.name}, age{that.age} {
            that.name = nullptr;
            that.age  = 0;
            cout << "Person{Person&& " << &that << "} @ " << this << endl;
        }

        auto operator=(const Person& that) -> Person& {
            cout << "operator =(const Person& " << &that << ") @ " << this << endl;
            if (this != &that) {
                delete[] name;
                name = copystr(that.name);
                age  = that.age;
            }
            return *this;
        }

        auto operator=(Person&& that) noexcept -> Person& {
            cout << "operator =(Person&& " << &that << ") @ " << this << endl;
            if (this != &that) {
                delete[] name;
                name = that.name;
                that.name = nullptr;
                age = that.age;
                that.age = 0;
            }
            return *this;
        }

        string toString() const {
            ostringstream buf{};
            buf << "Person(" << (name ? name : "??") << ", " << age << ") @ " << this;
            return buf.str();
        }

        unsigned incrAge() {
            return ++age;
        }
    };

}