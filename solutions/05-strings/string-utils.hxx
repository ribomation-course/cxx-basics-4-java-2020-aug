#include <string>

namespace ribomation::StringUtils {
    using std::string;

    extern auto lowercase(string) -> string;
    extern auto uppercase(string) -> string;
    extern auto strip(string) -> string;
    extern auto truncate(string, int ) -> string;

}
