#include "string-utils.hxx"

namespace ribomation::StringUtils {

    auto lowercase(string s) -> string {
        auto      r = string{};
        for (auto ch : s) r += tolower(ch);
        return r;
    }

    auto uppercase(string s) -> string {
        auto      r = string{};
        for (auto ch : s) r += toupper(ch);
        return r;
    }

    auto strip(string s) -> string {
        auto      r = string{};
        for (auto ch : s) if (isalpha(ch)) r += ch;
        return r;
    }

    auto truncate(string s, int w) -> string {
        if (s.size() <= w) return s;
        return s.substr(0, w);
    }

}

