#include <iostream>
#include <sstream>
#include <string>

using namespace std;
using namespace std::literals;

class Person {
    string   name = "no name";
    unsigned age  = 0;
public:
    Person(string name_, unsigned age_) : name{move(name_)}, age{age_} {
        cout << "Person{" << name << ", " << age << "} @ " << this << endl;
    }

    ~Person() {
        cout << "~Person{" << name << ", " << age << "} @ " << this << endl;
    }

    string toString() const {
        auto buf = ostringstream{};
        buf << "Person{" << name << ", " << age << "} @ " << this;
        return buf.str();
    }

    string getName() const { return name; }
    unsigned getAge() const { return age; }

    void setName(string name) { this->name = name; }
    void setAge(unsigned age) { Person::age = age; }
};

auto g = Person{"Per Silja", 52};
auto g2 = Person{"Inge Vidare", 52};

int main() {
    cout << "[main] enter\n";
    cout << "g: " << g.toString() << endl;

    {
        auto p = Person{"Cris P. Bacon", 42};
        cout << "p: " << p.toString() << endl;

        p.setName(p.getName() + ", 3rd");
        p.setAge(p.getAge() + 5);
        cout << "p: " << p.toString() << endl;

        auto h = new Person{"Justin Time", 62};
        cout << "h: " << h->toString() << endl;

        auto q = Person{"Anna Conda", 32};
        cout << "q: " << q.toString() << endl;

        h->setName(h->getName() + " jr.");
        h->setAge(h->getAge() + 3);
        cout << "h: " << h->toString() << endl;

        delete h;
    }
    cout << "[main] exit\n";
    return 0;
}
