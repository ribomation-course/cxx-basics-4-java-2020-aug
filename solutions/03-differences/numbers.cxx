#include "numbers.hxx"

auto sum(int n) -> long {
    if (n <= 0) return 0;
    return n + sum(n - 1);
}

auto prod(int n) -> long {
    if (n <= 0) return 1;
    return n * prod(n - 1);
}
