#include <iostream>
#include <string>
#include "numbers.hxx"

using namespace std;

int main(int argc, char** argv) {
    auto n = argc == 1 ? 5 : stoi(argv[1]);
    cout << "SUM(1.." << n << ")  = " << sum(n) << endl;
    cout << "PROD(1.." << n << ") = " << prod(n) << endl;
    return 0;
}
