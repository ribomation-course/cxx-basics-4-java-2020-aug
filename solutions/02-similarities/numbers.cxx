#include <iostream>
#include <limits>
using namespace std;

int main() {
    auto count = 0;
    auto sum   = 0.0;
    auto min   = numeric_limits<int>::max();
    auto max   = numeric_limits<int>::min();
    auto n     = 0;
    do {
        cout << "Number? ";
        cin >> n;
        if (n == 0) break;
        ++count;
        sum += n;
        min = n < min ? n : min;
        max = n > max ? n : max;
    } while (true);
    cout << "count  : " << count << endl;
    cout << "average: " << sum / count << endl;
    cout << "min    : " << min << endl;
    cout << "max    : " << max << endl;
    return 0;
}
