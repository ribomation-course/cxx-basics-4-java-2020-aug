#pragma once

#include <iostream>
#include <string>
#include <utility>

namespace ribomation {
    using std::string;
    using std::move;
    using std::cout;
    using std::endl;
    using std::ostream;

    class Person {
        const string name;
        unsigned     age;
        static int   instanceCount;

        static auto dump(string prefix, ostream& os, const Person& p) -> ostream& {
            return os << prefix << "(" << p.name << ", " << p.age << ") @ "
                      << &p << " (" << instanceCount << ")";
        }
    public:
        static int   getCount() { return instanceCount; }

        ~Person() {
            --instanceCount;
            dump("~Person", cout, *this) << endl;
        }

        Person() {
            ++instanceCount;
            dump("Person", cout, *this)<< endl;
        }

        Person(string n, unsigned a) : name{move(n)}, age{a} {
            ++instanceCount;
            dump("Person<string,int>", cout, *this)<< endl;
        }

        Person(const Person& that) : name{that.name}, age{that.age} {
            ++instanceCount;
            dump("Person<const Person&>", cout, *this)<< endl;
        }

        friend auto operator <<(ostream& os, const Person& p) -> ostream& {
            return dump("Person", os, p);
        }

        unsigned incrAge() {
            return ++age;
        }
    };

}

