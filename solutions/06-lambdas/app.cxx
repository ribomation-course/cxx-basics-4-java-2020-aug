#include <iostream>
#include <algorithm>
#include <functional>

using namespace std;

void modify(int* arr, int N, function<int(int)> f) {
    for (auto k = 0; k < N; ++k) arr[k] = f(arr[k]);
}

int main() {
    int        numbers[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    auto const N         = sizeof(numbers) / sizeof(numbers[0]);

    for_each(numbers, numbers + N, [](auto k) { cout << k << " "; });
    cout << endl;

    auto factor = 42;
    transform(numbers, numbers + N, numbers, [factor](auto k) { return k * factor; });
    for_each(numbers, numbers + N, [](auto k) { cout << k << " "; });
    cout << endl;

    modify(numbers, N, [](int k) -> int { return k * k; });
    for_each(numbers, numbers + N, [](auto k) { cout << k << " "; });
    cout << endl;

    return 0;
}
