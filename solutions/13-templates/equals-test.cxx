#include <iostream>
#include <string>
#include <cassert>
#include "equals.hxx"

using namespace std;
using namespace std::literals;

struct Person {
    const string name;
    explicit Person(string n) : name{move(n)} {}
    friend auto operator==(const Person& lhs, const Person& rhs) -> bool {
        return lhs.name == rhs.name;
    }
};

int main() {
    assert(equals(42, 40 + 2, 2 * 21));
    assert(equals("wifi"s, "wi"s + "fi"s, "**wifi**"s.substr(2, 4)));
    assert(equals(Person{"nisse"}, Person{"nisse"s}, Person{"nis"s + "se"s}));

    cout << "All tests passed\n";
    return 0;
}
